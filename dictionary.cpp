#include "dictionary.h"
#define DEBUG
#undef DEBUG

/**
 *	Inserts the entry into the Binary Tree recursively
 *
 *	parent - The parent entry
 *	entry - The entry to be added to the tree
 *	returns - True if the entry was successfully added to the tree,
 *				False otherwise
 */
bool InsertEntry(entry_t* parent, entry_t* entry) {
  if ((*entry).e.word > (*parent).e.word) {
    // Right child
    if ((*parent).rightChild == nullptr) {
      (*parent).rightChild = entry;
      return true;
    } else {
      return InsertEntry((*parent).rightChild, entry);
    }
  } else {
    // Left child
    if ((*parent).leftChild == nullptr) {
      (*parent).leftChild = entry;
      return true;
    } else {
      return InsertEntry((*parent).leftChild, entry);
    }
  }
  return false;
}

/**
 *	Adds the given word to the Binary Tree and the occurance thread.
 *
 *	dict - The dictionary to add the word to
 *	word - The word to be added to the dictionary
 *	returns - True if the word was successfully added, false if the tree is full or the
 *				word could not be added
 */
bool InsertWord(dict_t& dict, std::string word) {
  /*
    adds word to dictionary (tree and thread ), if word can't be added returns
    false else returns true
  */

  if (FullDictionary(dict)) {
    return false;
  }

  entry_t* head = dict.words;
  entry_t* entry = new entry_t;
  pair_t p = {1, word};
  (*entry).e = p;
  (*entry).next = nullptr;
  (*entry).prev = nullptr;

  dict.numWords++;

  bool success = false;
  if (dict.words == nullptr) {
    dict.words = entry;
    success = true;
  } else {
    success = InsertEntry(head, entry);
  }

  if (dict.wordLen == nullptr) {
    (*entry).next = nullptr;
    (*entry).prev = nullptr;
    dict.wordLen = entry;
  } else {
    (*entry).next = dict.wordLen;
    (*dict.wordLen).prev = entry;
    dict.wordLen = entry;
    (*entry).prev = nullptr;
  }

  return success;
}

/**
 *	Prints the Binary tree using in order traversal thus resulting in the
 *	dictionary being printed in alphabetical order
 *
 *	parent - The parent entry to be printed.
 */
void PrintInorder(entry_t* parent) {
  if (parent == nullptr)
    return;
  PrintInorder((*parent).leftChild);

  std::cout << (*parent).e.word << " " << (*parent).e.count << std::endl;

  PrintInorder((*parent).rightChild);
}

/**
 *	Prints the dictionary to the console recursively sorted in the
 *	order of number of occurances going from least to greatest
 *
 *	entry - The node to be printed, initialy the head of the occurances list
 */
void PrintOccurances(entry_t* entry) {
  if (entry == nullptr)
    return;
  std::cout << (*entry).e.word << ", " << (*entry).e.count << std::endl;

  PrintOccurances((*entry).next);
}

/**
 *	Outputs the dictionary to the console, printing the words first
 *	in alphabetical order, then once more in number of occurances.
 *
 *	dict - The dictionary to be dumped to console
 */
void DumpDictionary(dict_t& dict) {
  /*
    display the contents of dictionary in sorted order as well as dumping thread
    elements
  */

  PrintInorder(dict.words);
  std::cout << "-----------Occurances-----------" << std::endl;

  //Update the dictionary initial element
  entry_t* e = dict.wordLen;
  while ((*e).prev != nullptr) {
	  e = (*e).prev;
  }
  dict.wordLen = e;

  PrintOccurances(dict.wordLen);
}

/**
 *	Retrieves the next word in the text file. Words are separated by whitespace,
 *	symbols or numbers.
 *
 *	returns - The word in the form of a string
 */
std::string GetNextWord(void) {
  /*
     will retrieve next word in input stream. Word is defined just as in
     assignment #1 returns WORD or empty string if no more words in input stream
  */
  char c;
  bool newWord = true;

  std::string word;

  while (std::cin.good()) {
    // Read the input text one char at a time
    c = std::cin.get();

    // If this is a new word, initialize the word as an empty string
    if (newWord) {
      word = std::string();
      newWord = false;
    }

    // Convert the letter to lowercase and append to word
    if (isalpha(c)) {
      word += tolower(c);
    } else {
      newWord = true;
      if (word.length() > 0) {
        return word;
      }
    }
  }

  return std::string();
}

/**
 *	Determines whether the given dictionary has reached its
 *	maximum capacity
 *
 *	dict - The dictionary to be checked
 *	returns - True if the dictionary is full, false otherwise
 */
bool FullDictionary(dict_t& dict) {
  /*
     if dictionary is full, return true else false
   */
#ifndef DICT_NO_LIMIT
  return dict.numWords >= dict.maxEntries;
#else
	return false;
#endif
}

/**
 *	A helper function to search the tree recursively to find
 *	the target word
 *
 *	parent - The parent entry to be searched
 *	word - The word to be searched for
 *	returns - The pointer to the entry containing the 
 *				target word, nullptr if it could not be
 *				found
 */
entry_t* LocateEntry(entry_t* parent, std::string word) {
  if (parent == nullptr)
    return nullptr;
  if (word == (*parent).e.word) {
    return parent;
  } else if (word > (*parent).e.word) {
    if ((*parent).rightChild == nullptr) {
      return nullptr;
    } else {
      return LocateEntry((*parent).rightChild, word);
    }
  } else {
    if ((*parent).leftChild == nullptr) {
      return nullptr;
    } else {
      return LocateEntry((*parent).leftChild, word);
    }
  }
}

/**
 *	Searches the Binary Tree for the given word
 *
 *	dict - The dictionary to be searched
 *	word - The word to be searched for
 *	returns - A pointer to the entry containing the word, returns
 *				nullptr otherwise
 */
entry_t* LocateWord(dict_t& dict, std::string word) {
  /*
     will determine if dictionary contains word. if found, returns pointer to
     entry else returns  0
  */
  if (dict.words == nullptr)
    return nullptr;
  return LocateEntry(dict.words, word);
}

/**
 *	Swaps two entries in the form of a doubly linked list
 *
 *	a - The first entry to be swapped
 *	b - The second entry to be swapped
 */
void Swap(entry_t* a, entry_t* b) {
	// Prev(a) to b relationship
	if ((*a).prev != nullptr) {
		(*(*a).prev).next = b;
		(*b).prev = (*a).prev;
	} else {
		(*b).prev = nullptr;
	}

	// next(b) to a relationship
	if ((*b).next != nullptr) {
		(*(*b).next).prev = a;
		(*a).next = (*b).next;
	} else {
		(*a).next = nullptr;
	}

	(*a).prev = b;
	(*b).next = a;
}

/**
 *	Performs a bubble sort algorithm on the entries as they are updated
 *  using recursion
 *
 *	entry - The entry to be updated
 */
void UpdateList(entry_t* entry) {
	// Ensure that there is another entry to compare to
	if ((*entry).next != nullptr) {
		entry_t* next = (*entry).next;
		// If the current entry is larger than the next entry, swap
		// Perform action recursively
		if ((*entry).e.count > (*next).e.count) {
			// Perform swap and continue recursively
			Swap(entry, next);
			UpdateList(entry);
		}
	}
}