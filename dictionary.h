#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#ifndef DICT_NO_LIMIT
const int MAX = 100;
#else
const int MAX = -1;
#endif

/*
    structure describing a word entry in the dictionary
*/

typedef struct pair {
  int count;        /* frequency count for a particular word */
  std::string word; /* the word itself */
} pair_t;

typedef struct entry {
  pair_t e;
  struct entry* leftChild;  /* left sub-tree */
  struct entry* rightChild; /* right sub-tree */
  struct entry* next;       /* next sibling */
  struct entry* prev;       /* previous sibling */

} entry_t;

/*
    structure describing the dictionary
*/

typedef struct dict {
  int maxEntries; /* maximum number of entries allowed; this is an artificial
                     limit */
  /* linked lists can be as big as you want. This limit ensures that   */
  /* this code tries to behave like the previous ones */

  int numWords;     /* number of words in the dictionary */
  entry_t* words;   /* pointer to the entries in binary tree */
  entry_t* wordLen; /* pointer to entries in thread */
} dict_t;

/*
  you will have to write code for these 5 functions.
*/

entry_t* LocateWord(dict_t&, std::string);
bool FullDictionary(dict_t&);
bool InsertWord(dict_t&, std::string);
std::string GetNextWord(void);
void DumpDictionary(dict_t&);
void UpdateList(entry_t*);

/*
  note that these are global variables so that they are already initialized to 0
  do not write your functions so that they use these directly. All input
  variables MUST be passed as parameters!!!!
*/

static dict_t dictionary = {MAX, 0, nullptr, nullptr}; /* your dictionary */
static std::string word;
