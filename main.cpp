#include "dictionary.h"

int main(void) {
  entry_t* pos;

  while (1) {
    word = GetNextWord();

    if (word.empty()) {
      DumpDictionary(dictionary);
      break;
    }
    if ((pos = LocateWord(dictionary, word)) > 0) {
      pos->e.count++;
	  UpdateList(pos);
      // std::cout << word << std::endl;
    } else if (!InsertWord(dictionary, word)) {
      std::cout << "dictionary full \"" << word << "\" cannot be added\n";
    }
  }
  return 0;
}